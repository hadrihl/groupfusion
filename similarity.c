/*
 * filename: similarity.c
 * author: hadrihilmi@gmail.com pcom0315/11
 */

// include pre-define header
#include "struct.h"

// write SS's result into file
void writeToFile(SSVector *ranklist, int querySize) {
    
    FILE *file;
    int i, j;
    char name[40];

    for(i = 0; i < querySize; i++) {

        sprintf(name, "result/SS-%d.txt", i);
        file = fopen(name, "w+");
        
        for(j = 0; j < wombat; j++) {
            fprintf(file, "%d\t", ranklist[i].compound[j].id);
            fprintf(file, "%f\n", ranklist[i].compound[j].value);
        }

        fclose(file);
    }
}

// SS: tanimoto
int compareTani(TanVal *a, TanVal *b) {
    if(a->value > b->value) return -1;
    else return 1;
}

// non-binary Tanimoto: ECFC4 (count)

// sum of fragment counted for each compound.
float Sum_Prodv2(float *scr) {

    int i;
    int prod = 0.0;
    int sum_prod = 0.0;

    for(i = 0; i < NumBits; i++) {
        // ignore zero, directly calculate bits with value
        if(scr[i] > 0.000000) {
            prod = scr[i] * scr[i];
            sum_prod += prod;
        }
    }

    return (sum_prod);
}

// sum of common position between query and db compounds.
float Sum_ProdQDB(float *Qscr, float *DBscr) {
    int i;
    int prod = 0.0;
    int sum_prod = 0.0;

    for(i = 0; i < NumBits; i++) {
        if((Qscr[i] > 0) && (DBscr[i] > 0)) {
            prod = Qscr[i] * DBscr[i];
            sum_prod += prod;
        }
    }
    return (sum_prod);
}

// tanimoto
float tanimoto(float *query, float *db) {
    //float taniValue = 0,
    //      sum_prodQ = 0, 
    //      sum_prodDB = 0, 
    //      sum_prodQDB = 0;

    float sum_prodQ = (float)(Sum_Prodv2(query));
    float sum_prodDB = (float)(Sum_Prodv2(db));
    float sum_prodQDB = (float)(Sum_ProdQDB(query, db));

    float taniValue = sum_prodQDB / (sum_prodQ + sum_prodDB - sum_prodQDB);

    return (taniValue);
}

// similarity searching implementation
void run_ss(SSVector *ranklist, tScreen *qCompound, tScreen *dbCompound, int querySize) {

    // TODO
    int i, j;

    for(i = 0; i < querySize; i++) {
        
        for(j = 0; j < wombat; j++) {
            ranklist[i].compound[j].id = dbCompound[j].id;
            ranklist[i].compound[j].value = tanimoto(qCompound[i].screen, dbCompound[j].screen);
        }

        // sort score/rank
        //qsort(ranklist[i].compound, wombat, sizeof(TanVal), (compfn)compareTani);
    }

    // write SS's result into file
    writeToFile(ranklist, querySize);
}
