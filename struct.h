/*
 * filename: struct.h
 * author: hadrihilmi@gmail.com pcom0315/11
 */

#ifndef STRUCT_H
#define STRUCT_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#define NumBits 1024   /* Bitstring length: Usually Daylight: 1024 or 2048; Unity: 992; BCI: 1052 */
#define wombat 102540 /*  number of Compounds */
#define SIZE_OF_QUERY 10

typedef struct {
    int id;		            /* compound ID */
    int bitsset;			/* number of 'on' bits */
    float screen[NumBits];	/* Bitstring */
} tScreen;

// ----------- ss vector ----------------------
typedef struct {
	int id;
	float value;
} TanVal;

typedef struct {
    TanVal *compound;
} SSVector;

// ----------- fusion vector ------------------
typedef struct {
 int id;
 float simscore; 
 float RKP;	
} DBFuse;

typedef struct {
    DBFuse *compound;
} fuseVector;

// --------------------------------------------
typedef struct {
    int NONN;
    char name[120];	
} FileStruct;

typedef struct {
  int rank;
  int id;
  float value;
} InitRank;

typedef int (*compfn)(const void*, const void*);

void read_SSFile(TanVal *rankmolid, FILE *infile);
void run_ss(SSVector *ranklist, tScreen *qCompound, tScreen *dbCompound, int querySize);
void run_fusion(SSVector *ranklist, DBFuse *DBVec, int querySize, int opt);


#endif
