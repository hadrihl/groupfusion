/*
 * filename: fusion.c
 * author: hadrihilmi@gmail.com pcom0315/11
 */

// include, predefine header
#include "struct.h"

// used as sorting func
// fusion: max
int compareMAX(DBFuse *a, DBFuse *b) {
    if(a->simscore > b->simscore) return -1;
    else return 1;
}

// fusion: RKP
int compareRKP(DBFuse *a, DBFuse *b) {
    if(a->RKP > b->RKP) return -1;
    else return 1;
}

// sort fused list
void sort(DBFuse *DBVec) {
    // sort fused score/rank list
    qsort(DBVec, wombat, sizeof(DBFuse), (compfn)compareMAX);
}

// write to file
void writeFused(DBFuse *list, char *name) {

    sort(list);

    FILE *file;
    int i;
    char temp[40];

    sprintf(temp, "result/fused-%s.txt", name);
    file = fopen(temp, "w+");

    for(i = 0; i < wombat; i++) {
        // write rank, compound id, score
        fprintf(file, "%d\t%d\t", i + 1, list[i].id); // rank, compound id
        fprintf(file, "%f\n", list[i].simscore); // enable if score-based fusion
        // fprintf(file, "%f\n", list[i].RKP); // enable this if rank-based RKP
    }

    fclose(file);
}

// fusion: RKP rule
void fusedRKP(SSVector *ranklist, DBFuse *DBVec, int querySize) {
    printf("Stage: Fusion-RKP\n");

    int counter, i, j, rank;

    // for each ranklist file
    for(counter = 0; counter < querySize; counter++) {

        // for each compound in ranklist, find RKP
        for(i = 0; i < wombat; i++) {
            
            for(j = 0; j < wombat; j++) {

                if(DBVec[i].id == ranklist[counter].compound[j].id) {
                    if(DBVec[i].simscore < ranklist[counter].compound[j].value)
                        DBVec[i].simscore = ranklist[counter].compound[j].value;

                    // update rank
                    rank = j + 1;

                    if(ranklist[counter].compound[j].value == 0.00)
                        DBVec[i].RKP = DBVec[i].RKP + 0.00;
                    else
                        DBVec[i].RKP = DBVec[i].RKP + (float) 1 / (float) rank;
                    break;
                }
            }
        }//endRKP
    }
}

// fusion: MAX rule
void fusedMAX(SSVector *ranklist, DBFuse *DBVec, int querySize) {
    printf("Stage: Fusion-MAX\n");
    
    int i, j;

    // for each scorelist file
    for(i = 0; i < querySize; i++) {

        // for each compound in scorelist, find MAX
        for(j = 0; j < wombat; j++) {
            if(DBVec[j].simscore < ranklist[i].compound[j].value)
                DBVec[j].simscore = ranklist[i].compound[j].value;
        }//endMAX
    }
}

// TODO
void run_fusion(SSVector *ranklist, DBFuse *DBVec, int querySize, const int opt) {

    char *name = "";

    switch(opt) {
        case 1:
            fusedMAX(ranklist, DBVec, querySize);
            name = "MAX";
            break;
        case 2:
            fusedRKP(ranklist, DBVec, querySize);
            name = "RKP";
            break;
    };

    writeFused(DBVec, name);
}
