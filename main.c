/*
 * filename: main.c
 * author: hadrihilmi@gmail.com pcom0315/11
 */

// include, predefine header
#include <sys/time.h>
#include "struct.h" 

// read query version: 2
void read_queryfpv2(FILE *file, tScreen *queryCompound, int *size) {
    int row = 0, col, temp;

    while(fscanf(file, "%d", &temp) != EOF) {
        queryCompound[row].id = temp;

        for(col = 0; col < NumBits; col++)
            fscanf(file, "%f", &queryCompound[row].screen[col]);
        
        row += 1;
        
        while(getc(file) != '\n');
    }

    // return size of query
    *size = row;
    printf("Size of query: %d reference compound(s)\n", row);
}

// read database compounds
void read_dbfp(FILE *infile, tScreen *databaseCompounds) {
    int row, col;

    for(row = 0; row < wombat; row++) {
        fscanf(infile, "%d", &databaseCompounds[row].id);

        for(col = 0; col < NumBits; col++)
            fscanf(infile, "%f", &databaseCompounds[row].screen[col]);

        while(getc(infile) != '\n');
    }
}

// read SS file
void read_SSFile(TanVal *rankmolid, FILE *infile) {
    int i;

    for(i = 0; i < wombat; i++) {
        fscanf(infile, "%d", &rankmolid[i].id);
        fscanf(infile, "%f", &rankmolid[i].value);
    }
}

// preprocessing: query (reference compounds)
int prepareQuery(char *filename, tScreen *q) {

    FILE *file;
    int size;

    file = fopen(filename, "r");

    if(file == NULL)
        fprintf(stderr, "Error while opening file: %p\n", filename);
    else 
        read_queryfpv2(file, q, &size);

    fclose(file);

   return size;
}

// preprocessing: db compounds
void prepareDB(tScreen *db) {
    
    FILE *file;

    file = fopen("database/M_ECFC4_Database.txt", "r");

    if(file == NULL)
        fprintf(stderr, "Error while opening file: database/M_ECFC4_Database.txt\n");
    else
        read_dbfp(file, db);

    fclose(file);
}


/* main program */
int main(int argc, char **argv)
{
    struct timeval tz;
    struct timezone tx;
    double startTime, endTime;

    char *qfile;
    int querySize;

    // check passing arguments in console
    if(argc < 2) {
        printf("\nUsage: ./groupfusion <query-path>\ne.g: $./groupfusion query/query5HT1A.txt\n\n");
        return -1;

    } else qfile = argv[1];

    // variables init
    tScreen *queryCompound, *databaseCompounds;
    TanVal *TaniArr;
    DBFuse *DBVec;

    int row, i;

    // struct init
    queryCompound = (tScreen*) calloc(10, sizeof(tScreen));
    databaseCompounds = (tScreen*) calloc(wombat, sizeof(tScreen));
    TaniArr = (TanVal*) calloc(wombat, sizeof(TanVal));
    DBVec = (DBFuse*) calloc(wombat, sizeof(DBFuse));

    //----------- pre-processing ------------------------------------
    querySize = prepareQuery(qfile, queryCompound);

    prepareDB(databaseCompounds);

    // initialize vector used for fusion phase later
    for(row = 0; row < wombat; row++) {
        DBVec[row].simscore = 0.00;
        DBVec[row].RKP = 0.00;
    }

    // vector initialization
    SSVector *ranklist;

    ranklist = (SSVector*) calloc(querySize, sizeof(SSVector));

    for(i = 0; i < querySize; i++)
        ranklist[i].compound = TaniArr;

    //--------- similarity search ----------------------------------
    printf("Stage: Similarity Search\n");

    gettimeofday(&tz, &tx);
    startTime = (double) tz.tv_sec + (double) tz.tv_usec / 1000000.0;

    // TODO - TESTING
    run_ss(ranklist, queryCompound, databaseCompounds, querySize);

    gettimeofday(&tz, &tx);
    endTime = (double) tz.tv_sec + (double) tz.tv_usec / 1000000.0;
    printf("SS: %f seconds.\n", endTime - startTime);
    
    // ------------ fusion -----------------------------------------
    gettimeofday(&tz, &tx);
    startTime = (double) tz.tv_sec + (double) tz.tv_usec / 1000000.0;

    // @param 4: fused opt
    run_fusion(ranklist, DBVec, querySize, 1);

    //fusedRKP(DBVec, list, querySize); // apply RKP rule
    //fusedMAX(ranklist, DBVec, querySize); // apply MAX rule

    gettimeofday(&tz, &tx);
    endTime = (double) tz.tv_sec + (double) tz.tv_usec / 1000000.0;
    printf("fused: %f seconds.\n", endTime - startTime);

    // clean up memory
    free(ranklist);
    free(queryCompound);
    free(databaseCompounds);
    free(TaniArr);
    free(DBVec);

    return 0;
}
