CC      = gcc
CFLAGS  = -Wall -g -O2

BIN =  groupfusion

all: $(BIN)

main_cc: main.c fusion.c similarity.c
	$(CC) -c $(CFLAGS) -o $@ $<

groupfusion: main.o fusion.o similarity.o
	$(CC) $(CFLAGS) -o $@ $^

clean:
	$(RM) *.o $(BIN) result/*.txt
