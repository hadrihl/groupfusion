groupfusion
===========
A virtual screening app for similarity-based Group Fusion

TODO list
---------
* enable support for CUDA

Changes
-------
* program runs as sequential by default


mark:
The program is built using one Makefile in order to make ease of development. 
Particular flags are enabled through compiling (For more details, check Makefile).

Compile
-------
$ make

Run the program
---------------
$ groupfusion "query-path"

e.g:
$ ./groupfusion query/query-5HT1A.txt


Appendix
========

Pseudocode: sequential
----------------------
for each reference compound
	perform similarity search
	write score/rank to list
endfor

for each score/rank lists
	fused the list
endfor
